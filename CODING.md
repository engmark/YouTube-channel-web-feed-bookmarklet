# Coding instructions

## Development setup

1. [Install and enable `pyenv`](https://github.com/pyenv/pyenv#installation)
1. Install the project Python version: `pyenv install`
1. Restart your shell: `exec "$SHELL"`
1. Verify setup: `diff <(python <<< 'import platform; print(platform.python_version())') .python-version` – should produce no output.
1. [Install and enable Poetry](https://python-poetry.org/docs/#installation)
1. Run `./reset-dev-env.bash` to install packages.

Re-run `./reset-dev-env.bash` when packages change.

## Test

```shell
make selenium # Unless already running
make test
```

Run `make LOGLEVEL=DEBUG test` to log more during tests. See [Python log levels](https://docs.python.org/3/library/logging.html#logging-levels).
